package com.example.RESTfulCrudAPI.repository;

import com.example.RESTfulCrudAPI.model.Question;
import org.springframework.data.jpa.repository.JpaRepository;

public interface QuestionRepository extends JpaRepository<Question, Long> {
}
