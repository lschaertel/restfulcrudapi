package com.example.RESTfulCrudAPI;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class ResTfulCrudApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(ResTfulCrudApiApplication.class, args);
    }
}
